package com.amit.hn.utils;

public final class Constant {

	public static final String BASE_URL = "https://hacker-news.firebaseio.com/v0/";
	public static final String BEST_STORIES = "beststories.json?orderBy=\"$key\"&limitToFirst=10";
	public static final String ITEM = "item/{id}.json";
	public static final String USER = "user/{id}.json";
}
