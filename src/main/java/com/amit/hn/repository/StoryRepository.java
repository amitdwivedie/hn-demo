package com.amit.hn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amit.hn.domain.Story;

public interface StoryRepository extends JpaRepository<Story, Integer> {

	List<Story> findAllByOrderByScoreDesc();


	
}
