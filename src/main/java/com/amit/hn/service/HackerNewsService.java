package com.amit.hn.service;

import static com.amit.hn.utils.Constant.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.amit.hn.domain.Comment;
import com.amit.hn.domain.Item;
import com.amit.hn.domain.Item.Type;
import com.amit.hn.domain.Story;
import com.amit.hn.domain.User;
import com.amit.hn.task.CommentCallable;
import com.amit.hn.task.StoryCallable;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

@Service
public class HackerNewsService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private ExecutorService executor = Executors.newFixedThreadPool(10);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private StoryService storyService;

	private LoadingCache<String, List<Integer>> bestStoriesCache = CacheBuilder.newBuilder()
			.expireAfterWrite(15, TimeUnit.MINUTES).build(new CacheLoader<String, List<Integer>>() {
				@Override
				public List<Integer> load(String key) throws Exception {
					return getBestStoriesId();
				}
			});

	private LoadingCache<Integer, Item> itemCache = CacheBuilder.newBuilder().expireAfterWrite(15, TimeUnit.MINUTES)
			.build(new CacheLoader<Integer, Item>() {
				@Override
				public Item load(Integer key) throws Exception {
					return null;
				}
			});

	private LoadingCache<String, User> userCache = CacheBuilder.newBuilder().expireAfterWrite(15, TimeUnit.MINUTES)
			.build(new CacheLoader<String, User>() {
				@Override
				public User load(String key) throws Exception {
					return getUser(key);
				}
			});

	@SuppressWarnings("unchecked")
	public List<Integer> getBestStoriesId() {
		log.debug("Get Best stories ids");
		return restTemplate.getForObject(BASE_URL + BEST_STORIES, List.class);
	}

	public List<Story> getBestStories() throws ExecutionException {
		long start = System.currentTimeMillis();
		List<Story> stories = new ArrayList<Story>();
		List<Integer> storyIds = bestStoriesCache.get("best_stories");
		if (storyIds != null && !storyIds.isEmpty()) {
			List<Future<Story>> list = new ArrayList<Future<Story>>();
			for (Integer storyId : storyIds) {
				StoryCallable callable = new StoryCallable(storyId, restTemplate, itemCache, storyService);
				Future<Story> futureStory = executor.submit(callable);
				list.add(futureStory);
			}
			for (Future<Story> futureStory : list) {
				try {
					Story story = futureStory.get();
					stories.add(story);
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		}
		log.debug("Time taken to get best stories {} ", (System.currentTimeMillis() - start));
		return stories;
	}

	public Item getItem(Integer storyId) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", storyId);
		Item item = restTemplate.getForObject(BASE_URL + ITEM, Item.class, params);
		return item;
	}

	public Item getStory(Integer storyId) throws ExecutionException {
		return itemCache.getIfPresent(storyId);
	}

	public List<Comment> getCommentsForStory(Integer storyId) throws ExecutionException {
		long start = System.currentTimeMillis();
		List<Comment> comments = new ArrayList<Comment>();
		Item item = itemCache.getIfPresent(storyId);
		if (item == null) {
			item = getItem(storyId);
			itemCache.put(storyId, item);
		}
		if (item == null) {
			throw new RuntimeException("Story not found: " + storyId);
		}
		if (item.getType() != Type.story) {
			throw new RuntimeException("Item is not story: " + storyId);
		}
		List<Integer> kids = item.getKids();
		List<Integer> commentsId = new ArrayList<Integer>(kids.subList(0, 10));
		if (commentsId != null && !commentsId.isEmpty()) {
			List<Future<Comment>> list = new ArrayList<Future<Comment>>();
			for (Integer commentId : commentsId) {
				CommentCallable callable = new CommentCallable(commentId, restTemplate, itemCache, userCache);
				Future<Comment> futureComment = executor.submit(callable);
				list.add(futureComment);
			}
			for (Future<Comment> futureComment : list) {
				try {
					Comment comment = futureComment.get();
					comments.add(comment);
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}

		}
		log.debug("Time taken by story's comments {} ", (System.currentTimeMillis() - start));
		return comments;
	}

	public User getUser(String userId) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", userId);
		User user = restTemplate.getForObject(BASE_URL + USER, User.class, params);
		return user;
	}

}
