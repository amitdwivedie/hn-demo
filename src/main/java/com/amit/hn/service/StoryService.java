package com.amit.hn.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.amit.hn.domain.Story;
import com.amit.hn.repository.StoryRepository;

@Service
@Transactional
public class StoryService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private StoryRepository storyRepository;

	@Async
	public void saveStory(Story story) {
		storyRepository.save(story);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("Story saved: {} ", story.getId());
	}

	public List<Story> findAll() {
		List<Story> stories = storyRepository.findAllByOrderByScoreDesc();
		return stories;
	}

}
