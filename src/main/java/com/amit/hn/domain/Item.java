package com.amit.hn.domain;

import java.util.List;

public class Item {

	private Integer id;

	private Boolean deleted;

	private Type type;

	private String by;

	private Long time;

	private String text;

	private Boolean dead;

	private Integer parent;

	private Integer poll;

	private List<Integer> kids;

	private String url;

	private Integer score;

	private String title;

	private List<Integer> parts;

	private Long descendants;

	public enum Type {

		job("job"), story("story"), comment("comment"), poll("poll"), pollopt("pollopt");

		private final String name;

		private Type(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getBy() {
		return by;
	}

	public void setBy(String by) {
		this.by = by;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getDead() {
		return dead;
	}

	public void setDead(Boolean dead) {
		this.dead = dead;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public Integer getPoll() {
		return poll;
	}

	public void setPoll(Integer poll) {
		this.poll = poll;
	}

	public List<Integer> getKids() {
		return kids;
	}

	public void setKids(List<Integer> kids) {
		this.kids = kids;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Integer> getParts() {
		return parts;
	}

	public void setParts(List<Integer> parts) {
		this.parts = parts;
	}

	public Long getDescendants() {
		return descendants;
	}

	public void setDescendants(Long descendants) {
		this.descendants = descendants;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", deleted=" + deleted + ", type=" + type + ", by=" + by + ", time=" + time
				+ ", text=" + text + ", dead=" + dead + ", parent=" + parent + ", poll=" + poll + ", kids=" + kids
				+ ", url=" + url + ", score=" + score + ", title=" + title + ", parts=" + parts + ", descendants="
				+ descendants + "]";
	}

}
