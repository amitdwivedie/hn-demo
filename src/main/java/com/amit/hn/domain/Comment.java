package com.amit.hn.domain;

public class Comment {

	private Integer id;

	private String text;

	private String username;

	private Integer karma;

	private Integer userSince;

	public Comment() {
		
	}
	
	public Comment(Integer id, String text, Integer karma, int userSince, String username) {
		this.id = id;
		this.text = text;
		this.karma = karma;
		this.userSince = userSince;
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getKarma() {
		return karma;
	}

	public void setKarma(Integer karma) {
		this.karma = karma;
	}

	public Integer getUserSince() {
		return userSince;
	}

	public void setUserSince(Integer userSince) {
		this.userSince = userSince;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", text=" + text + ", username=" + username + ", karma=" + karma + ", userSince="
				+ userSince + "]";
	}

}
