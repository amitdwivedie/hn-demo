package com.amit.hn.rest;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.amit.hn.domain.Comment;
import com.amit.hn.domain.Story;
import com.amit.hn.service.HackerNewsService;
import com.amit.hn.service.StoryService;

@RestController
public class HackerNewsResource {

	@Autowired
	private HackerNewsService hackerNewsService;

	@Autowired
	private StoryService storyService;

	@GetMapping("/best-stories")
	public ResponseEntity<List<Story>> getBestStories() throws ExecutionException {
		List<Story> stories = hackerNewsService.getBestStories();
		return new ResponseEntity<>(stories, HttpStatus.OK);
	}

	@GetMapping("/past-stories")
	public ResponseEntity<List<Story>> getPastStories() {
		List<Story> stories = storyService.findAll();
		return new ResponseEntity<>(stories, HttpStatus.OK);
	}

	@GetMapping("/comments/{storyId}")
	public ResponseEntity<List<Comment>> getComments(@PathVariable Integer storyId) throws ExecutionException {
		List<Comment> comments = hackerNewsService.getCommentsForStory(storyId);
		return new ResponseEntity<>(comments, HttpStatus.OK);
	}
}
