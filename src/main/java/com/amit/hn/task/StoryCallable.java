package com.amit.hn.task;

import static com.amit.hn.utils.Constant.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.springframework.web.client.RestTemplate;

import com.amit.hn.domain.Item;
import com.amit.hn.domain.Story;
import com.amit.hn.service.StoryService;
import com.google.common.cache.LoadingCache;

public class StoryCallable implements Callable<Story> {

	private Integer itemId;
	private RestTemplate restTemplate;
	private LoadingCache<Integer, Item> itemCache;
	private StoryService storyService;

	public StoryCallable(Integer itemId, RestTemplate restTemplate, LoadingCache<Integer, Item> itemCache,
			StoryService storyService) {
		this.itemId = itemId;
		this.restTemplate = restTemplate;
		this.itemCache = itemCache;
		this.storyService = storyService;
	}

	@Override
	public Story call() throws Exception {
		boolean isCached = true;
		Item item = itemCache.getIfPresent(itemId);
		if (item == null) {
			item = getItem(itemId);
			isCached = false;
			itemCache.put(itemId, item);
		}
		Story story = getStoryFromItem(item);
		if (!isCached) {
			storyService.saveStory(story);
		}
		return story;
	}

	private Item getItem(Integer storyId) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", storyId);
		Item item = restTemplate.getForObject(BASE_URL + ITEM, Item.class, params);
		return item;
	}

	private Story getStoryFromItem(Item item) {
		Story story = new Story();
		story.setId(item.getId());
		story.setBy(item.getBy());
		story.setScore(item.getScore());
		story.setTitle(item.getTitle());
		story.setUrl(item.getUrl());
		story.setTime(item.getTime());
		return story;

	}

}
