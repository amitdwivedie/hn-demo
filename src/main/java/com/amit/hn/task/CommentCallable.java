package com.amit.hn.task;

import static com.amit.hn.utils.Constant.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.springframework.web.client.RestTemplate;

import com.amit.hn.domain.Comment;
import com.amit.hn.domain.Item;
import com.amit.hn.domain.User;
import com.google.common.cache.LoadingCache;

public class CommentCallable implements Callable<Comment> {

	private Integer itemId;
	private RestTemplate restTemplate;
	private LoadingCache<Integer, Item> itemCache;
	private LoadingCache<String, User> userCache;

	public CommentCallable(Integer itemId, RestTemplate restTemplate, LoadingCache<Integer, Item> itemCache,
			LoadingCache<String, User> userCache) {
		this.itemId = itemId;
		this.restTemplate = restTemplate;
		this.itemCache = itemCache;
		this.userCache = userCache;
	}

	@Override
	public Comment call() throws Exception {
		Item item = itemCache.getIfPresent(itemId);
		if (item == null) {
			item = getItem(itemId);
			itemCache.put(itemId, item);
		}
		Comment story = getStoryFromItem(item);
		return story;
	}

	private Item getItem(Integer storyId) {
		Map<String, Object> params = new HashMap<>();
		params.put("id", storyId);
		Item item = restTemplate.getForObject(BASE_URL + ITEM, Item.class, params);
		return item;
	}

	private Comment getStoryFromItem(Item item) {
		User user;
		try {
			user = userCache.get(item.getBy());
			return new Comment(item.getId(), item.getText(), user.getKarma(), getUserCreated(user.getCreated()),
					user.getId());
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return null;

	}

	private int getUserCreated(Long created) {
		LocalDate localDateTime = Instant.ofEpochSecond(created).atZone(ZoneId.systemDefault()).toLocalDate();
		return Period.between(localDateTime, LocalDate.now()).getYears();
	}

}
